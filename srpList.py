import json


class SrpList:

    def __init__(self):
        with open("ships.json", "r") as json_file:
            self.ships = json.load(json_file)

    def ship_exists(self, ship_name):
        return ship_name in self.ships.keys()

    def module_exists(self, ship_name, module_name):
        return module_name in self.ships[ship_name]["fit"]

    def percent(self, ship_name):
        return self.ships[ship_name]["percent"]
