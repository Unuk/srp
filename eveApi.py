import urllib.request
import json


def get_json(path):
    with urllib.request.urlopen(path) as url:
        return json.loads(url.read().decode())


class Esi:
    def __init__(self):
        self.base_url = "https://esi.evetech.net/latest"

    def get_type_name(self, type_id):
        return get_json("{}/universe/types/{}".format(self.base_url, type_id))["name"]

    def get_price(self, type_id):
        region_id = 10000002  # the forge
        orders = get_json("{}/markets/{}/orders/?datasource=tranquility&order_type=sell&page=1&type_id={}"
                          .format(self.base_url, region_id, type_id))
        if len(orders) == 0:
            return 0
        region_minimum_price = orders[0]["price"]
        jita_minimum_price = 0
        for order in orders:
            if order["price"] < region_minimum_price:
                region_minimum_price = order["price"]
            if order["location_id"] == 60003760:  # jita 4-4 station
                if jita_minimum_price == 0 or order["price"] < jita_minimum_price:
                    jita_minimum_price = order["price"]
        if jita_minimum_price > 0:
            return jita_minimum_price
        else:
            return region_minimum_price

    def get_insurance(self, ship_id):
        insurances = get_json("{}/insurance/prices/?datasource=tranquility&language=en-us".format(self.base_url))
        for insurance in insurances:
            if insurance["type_id"] == ship_id:
                for level in insurance["levels"]:
                    if level["name"] == "Platinum":
                        return level["payout"] - level["cost"]
        return 0

    def get_character_name(self, character_id):
        character = get_json("{}/characters/{}/?datasource=tranquility".format(self.base_url, character_id))
        return character["name"]
